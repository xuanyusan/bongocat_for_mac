const db = require("../../../src/main/database");

describe("[lowdb]模块的测试", function() {
  it('data-get', function(done) {
    console.log(db.get('userDefineShell').find({label: "Second"}).value());
    done();
	});
  it('data-get-error', function(done) {
    console.log(db.get('userDefineShell').find({label: "12"}));
    done();
	});
  it.skip('data-init', function(done) {
    db.defaults({users: []}).write();
	});
  it.skip('data-put', function(done) {
    db.get('posts')
    .push({id: 1, title: 'lowdb is awesome'})
    .write()
  });
});