'use strict';

const events = {
  3: 'keypress',
  4: 'keydown',
  5: 'keyup',
  6: 'mouseclick',
  7: 'mousedown',
  8: 'mouseup',
  9: 'mousemove',
  10: 'mousedrag',
  11: 'mousewheel',
};

describe("[iohook]模块 mouseevent 的测试", function() {
	it('ioHook_mousemove', function(done) {
		try {
			const ioHook = require('iohook');
			ioHook.start(false);
			let mousemove_handle = function (res) {
				console.log("\tmousemove", res);
				ioHook.off("mousemove", mousemove_handle);
				ioHook.stop();
				done();
			};
			ioHook.on("mousemove", mousemove_handle);
		} catch (error) {
			done(error.split('\n')[0])
		}
	});
	it('ioHook_mousedown', function(done) {
		try {
			const ioHook = require('iohook');
			ioHook.start(false);
			let mousedown_handle = function (res) {
				console.log("\tmousedown", res);
				ioHook.off("mousedown", mousedown_handle);
				ioHook.stop();
				done();
			};
			ioHook.on("mousedown", mousedown_handle);
		} catch (error) {
			done(error.split('\n')[0])
		}
	});
	it('ioHook_mouseup', function(done) {
		try {
			const ioHook = require('iohook');
			ioHook.start(false);
			let mouseup_handle = function (res) {
				console.log("\tmouseup", res);
				ioHook.off("mouseup", mouseup_handle);
				ioHook.stop();
				done();
			};
			ioHook.on("mouseup", mouseup_handle);
		} catch (error) {
			done(error.split('\n')[0])
		}
	});
	it('ioHook_mouseclick', function(done) {
		try {
			const ioHook = require('iohook');
			ioHook.start(false);
			let mouseclick_handle = function (res) {
				console.log("\tmouseclick", res);
				ioHook.off("mouseclick", mouseclick_handle);
				ioHook.stop();
				done();
			};
			ioHook.on("mouseclick", mouseclick_handle);
		} catch (error) {
			done(error.split('\n')[0])
		}
	});
	it('ioHook_mousedrag', function(done) {
		try {
			const ioHook = require('iohook');
			ioHook.start(false);
			let mousedrag_handle = function (res) {
				console.log("\tmousedrag", res);
				ioHook.off("mousedrag", mousedrag_handle);
				ioHook.stop();
				done();
			};
			ioHook.on("mousedrag", mousedrag_handle);
		} catch (error) {
			done(error.split('\n')[0])
		}
	});
	it('ioHook_mousewheel', function(done) {
		try {
			const ioHook = require('iohook');
			ioHook.start(false);
			let mousewheel_handle = function (res) {
				console.log("\tmousewheel", JSON.stringify(res));
				ioHook.off("mousewheel", mousewheel_handle);
				ioHook.stop();
				done();
			};
			ioHook.on("mousewheel", mousewheel_handle);
		} catch (error) {
			done(error.split('\n')[0])
		}
	});
});

describe("[iohook]模块 keyevent 的测试", function() {
	// 在node环境下，只能测试shift键
	it('ioHook_keydown', function(done) {
		try {
			const ioHook = require('iohook');
			ioHook.start(false);
			let keydown_handle = function (res) {
				console.log("\tkeydown", JSON.stringify(res));
				ioHook.off("keydown", keydown_handle);
				ioHook.stop();
				done();
			};
			ioHook.on("keydown", keydown_handle);
		} catch (error) {
			done(error.split('\n')[0])
		}
	});
	// 在node环境下，无法测试
	it.skip('ioHook_keyup', function(done) {
		try {
			const ioHook = require('iohook');
			ioHook.start(false);
			let keyup_handle = function (res) {
				console.log("\tkeyup", res);
				ioHook.off("keyup", keyup_handle);
				ioHook.stop();
				done();
			};
			ioHook.on("keyup", keyup_handle);
		} catch (error) {
			done(error.split('\n')[0])
		}
	});
	// 在node环境下，无法测试
	it.skip('ioHook_keypress', function(done) {
		try {
			const ioHook = require('iohook');
			ioHook.start(false);
			let keypress_handle = function (res) {
				console.log("\tkeypress", res);
				ioHook.off("keypress", keypress_handle);
				ioHook.stop();
				done();
			};
			ioHook.on("keypress", keypress_handle);
		} catch (error) {
			done(error.split('\n')[0])
		}
	});
});