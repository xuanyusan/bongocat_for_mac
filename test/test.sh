if [ -z $1 ];then
node_modules/.bin/mocha test/**/*.test.js -u bdd --timeout 10000 --colors
else
node_modules/.bin/mocha test/**/*.test.js -u bdd --grep $1 --timeout 10000 --colors
fi 