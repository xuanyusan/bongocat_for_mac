const { BrowserWindow } = require('electron');

function windowCloseController() {
  let window = BrowserWindow.getFocusedWindow();
  window && window.close();
}

module.exports = {
  windowCloseController
}
