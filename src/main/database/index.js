const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');  // 有多种适配器可选择
const path = require("path");
const adapter = new FileSync(path.join(__dirname, './data/db.json')); // 申明一个适配器
const db = low(adapter);

module.exports = db;