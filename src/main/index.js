const { app, ipcMain } = require('electron');
const { appReadyController, appBeforeQuitController } = require("./controller/appController")
const {
  shellTestController,
  shellPutController,
  shellDeleteController,
  shellDataGetController,
  shellStatusgetController
} = require("./controller/shellAdminController");
const { windowCloseController } = require("./controller/baseContoller")

ipcMain.on("shell-test", shellTestController);
ipcMain.on('shell-put', shellPutController);
ipcMain.on('shell-delete', shellDeleteController);
ipcMain.on('window-close', windowCloseController);
ipcMain.on('shell-data-get', shellDataGetController);
ipcMain.on('tip-change-active-shell', shellStatusgetController);

app.dock.hide();

app.on('ready', appReadyController);

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  // if (mainWindow === null) createWindow()
})

app.on('before-quit', appBeforeQuitController);