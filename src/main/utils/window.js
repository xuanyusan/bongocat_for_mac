const { BrowserWindow } = require('electron');

function createTransparentWindow(obj) {
	let baseObj = {
		x: 0,
		y: 0,
		width: 370,
		height: 410,
		// 固定大小窗口
		maximizable: false,
		minimizable: false,
		resizable: false,
		fullscreenable: false,
		// 无边框、无阴影、透明、置顶窗口
		frame: false,
		transparent: true,
		hasShadow: false,
		alwaysOnTop: true,
		titleBarStyle: 'customButtonsOnHover',
		// 集成node
		webPreferences: {
			nodeIntegration: true,
			contextIsolation: false,
			enableRemoteModule: true
		}
	};
	for (let key in obj) {
		if (key in baseObj) baseObj[key] = obj[key];
	}
	return new BrowserWindow(baseObj);
}
function createWindow(obj) {
	let baseObj = {
		x: 0,
		y: 0,
		width: 600,
		height: 450,
		// 固定大小窗口
		maximizable: false,
		minimizable: false,
		resizable: false,
		fullscreenable: false,
		// 无边框、无阴影、透明窗口
		frame: false,
		transparent: true,
		titleBarStyle: 'customButtonsOnHover',
		// 集成node
		webPreferences: {
			nodeIntegration: true,
			contextIsolation: false,
			enableRemoteModule: true
		}
	};
	for (let key in obj) {
		if (key in baseObj) baseObj[key] = obj[key];
	}
	return new BrowserWindow(baseObj);
}

module.exports = {
	createTransparentWindow,
	createWindow
}