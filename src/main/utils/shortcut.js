const ioHook = require("iohook");
const { BrowserWindow } = require('electron');

function commonShortcutRegister(window) {
  if(process.platform === "darwin") {
    let contents = window.webContents;
    ioHook.start(false);
    ioHook.on("keydown", (res) => {
      if(BrowserWindow.getFocusedWindow() == window) {
        contents.send(res);
        if(res.keycode == 23 && res.metaKey && res.shiftKey) {
          contents.openDevTools();
        }
      }
    });
  }
}

module.exports = {
  commonShortcutRegister
}