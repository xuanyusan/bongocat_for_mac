const spawn = require("child_process");
const program = require("../common/program");
const { constant } = require("../database");

let execShell = function(item, appTray){
  let reg = RegExp(item.resultReg);
  item.isUp && appTray.setTitle(` \x1B[${item.color}m${item.upMessage}\x1B[0m`);
  let children = spawn.exec(item.clickShell, function(error, stdout, stderr){
    let index = program.shellChild.findIndex(item=>item == children);
    if(index > -1) {
      program.shellChild.splice(index, 1);
    }
    if (error !== null) {
      console.log('exec error: ' + stderr);
      return
    }
    let content = stdout.match(reg);
    !item.isUp && appTray.setTitle(` \x1B[${item.color}m${content?content[0]:"error"}\x1B[0m`);
  });
  return children;
};

module.exports = {
  execShell
}