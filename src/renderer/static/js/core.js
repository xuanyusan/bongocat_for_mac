const charlines = [
  {
    base: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '='],
    shift: ['!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+']
  }, {
    base: ['⇥', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '↵'],
    shift: ['⇤', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '{', '}', '↵']
  }, {
    base: ['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\''],
    shift: ['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':', '"']
  }, {
    base: ['z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/'],
    shift: ['Z', 'X', 'C', 'V', 'B', 'N', 'M', '<', '>', '?']
  }
]

const special = [
  'esc', 'tab', 'caps', 'shift', 'fn', 'ctrl', 'alt', 'command', 'space', 'delete', 'enter'
]

const code2char = (code, shift, flag) => {
  if (code == 1) {
    return !flag ? '' : special[0];
  } else if (code >= 2 && code <= 13) {
    return shift ? charlines[0].shift[code - 2] : charlines[0].base[code - 2];
  } else if (code >= 15 && code <= 28) {
    return shift ? charlines[1].shift[code - 15] : charlines[1].base[code - 15];
  } else if (code == 29) {
    return !flag ? '' : special[5];
  } else if (code >= 30 && code <= 40) {
    return shift ? charlines[2].shift[code - 30] : charlines[2].base[code - 30];
  } else if (code == 41) {
    return shift ? '~' : '`';
  } else if (code == 42) {
    return !flag ? '' : special[3];
  } else if (code >= 44 && code <= 53) {
    return shift ? charlines[3].shift[code - 44] : charlines[3].base[code - 44];
  }
  return ''
};

function fillRoundRect(cxt, x, y, width, height, radius, fillColor) {
  if (2 * radius > width || 2 * radius > height) {
    radius = width < height ? width / 2 : height / 2;
  }
  cxt.save();
  cxt.translate(x, y);
  drawRoundRectPath(cxt, width, height, radius);
  cxt.fillStyle = fillColor || "#000";
  cxt.fill();
  cxt.restore();
}


function strokeRoundRect(cxt, x, y, width, height, radius, lineWidth, strokeColor) {
  if (2 * radius > width || 2 * radius > height) {
    radius = width < height ? width / 2 : height / 2;
  }
  cxt.save();
  cxt.translate(x, y);
  drawRoundRectPath(cxt, width, height, radius);
  cxt.lineWidth = lineWidth || 2;
  cxt.strokeStyle = strokeColor || "#000";
  cxt.stroke();
  cxt.restore();
}

function drawRoundRectPath(cxt, width, height, radius) {
  cxt.beginPath(0);
  cxt.arc(width - radius, height - radius, radius, 0, Math.PI / 2);
  cxt.lineTo(radius, height);
  cxt.arc(radius, height - radius, radius, Math.PI / 2, Math.PI);
  cxt.lineTo(0, radius);
  cxt.arc(radius, radius, radius, Math.PI, Math.PI * 3 / 2);
  cxt.lineTo(width - radius, 0);
  cxt.arc(width - radius, radius, radius, Math.PI * 3 / 2, Math.PI * 2);
  cxt.lineTo(width, height - radius);
  cxt.closePath();
}

const { ipcRenderer } = require("electron");

const models = [{
  ctx: null,
  mouse_res: { x: 0, y: 0, width: 1, height: 1 },
  timer: -1,
  initModel() {
    let canvas = document.createElement("canvas");
    canvas.id = "keyboard";
    document.body.appendChild(canvas);
    this.ctx = canvas.getContext("2d");
    fillRoundRect(this.ctx, 0, 0, 120, 140, 20, "#2e4e7e90");
    fillRoundRect(this.ctx, 140, 0, 150, 150, 20, "#2e4e7e");
    ipcRenderer.on("mouseevent", this.mouse_event.bind(this), false);
    this.timer = setInterval(_ => {
      this.ctx.font = '40px "微软雅黑"';
      this.ctx.fillStyle = "red";
      this.ctx.textBaseline = "top";
      this.ctx.textAlign = 'left';
      fillRoundRect(this.ctx, 140, 0, 150, 150, 20, "#2e4e7e50");
      this.ctx.fillText('·', 140 + this.mouse_res.x / this.mouse_res.width * 120, this.mouse_res.y / this.mouse_res.height * 120);
    }, 50);
    ipcRenderer.on("keyevent", this.key_event.bind(this));
  },
  dropModel() {
    ipcRenderer.off("mouseevent", this.mouse_event);
    ipcRenderer.off("keyevent", this.key_event);
    clearInterval(this.timer);
    this.ctx = null;
    this.mouse_res = { x: 0, y: 0, width: 1, height: 1 };
    this.timer = -1;
  },
  mouse_event(_, res) {
    this.mouse_res = res;
  },
  key_event(_, e) {
    if (e.type == "keyup") {
      this.ctx.clearRect(0, 0, 120, 140);
      fillRoundRect(this.ctx, 0, 0, 120, 140, 20, "#2e4e7e90");
    } else {
      this.ctx.font = '90px "微软雅黑"';
      this.ctx.fillStyle = "#D8D8D8";
      this.ctx.textBaseline = "top";
      this.ctx.textAlign = "center";
      this.ctx.fillText(code2char(e.keycode, e.shiftKey), 60, 20);
    }
  }
}, {
  initModel() {
    let bailaoban_base = document.createElement("div");
    bailaoban_base.id = "bailaoban_base";
    bailaoban_base.className = "bailaoban";
    document.body.appendChild(bailaoban_base);
    let bailaoban_right_hand = document.createElement("div");
    bailaoban_right_hand.id = "bailaoban_right_hand";
    bailaoban_right_hand.className = "bailaoban";
    document.body.appendChild(bailaoban_right_hand);

    ipcRenderer.on("keyevent", this.key_event.bind(this));
  },
  key_event(sender, e) {
    if (e.type == "keyup") {
      document.getElementById("bailaoban_right_hand").style.backgroundPositionX = 0 + "px"
    } else {
      // console.log(e);
      if (e.keycode == 57416 || e.keycode == 17) {
        // 上
        document.getElementById("bailaoban_right_hand").style.backgroundPositionX = 366 * 3 + "px"
      } else if (e.keycode == 57424 || e.keycode == 31) {
        // 下
        document.getElementById("bailaoban_right_hand").style.backgroundPositionX = 366 * 4 + "px"
      } else if (e.keycode == 57419 || e.keycode == 30) {
        // 左
        document.getElementById("bailaoban_right_hand").style.backgroundPositionX = 366 * 2 + "px"
      } else if (e.keycode == 57421 || e.keycode == 32) {
        // 右
        document.getElementById("bailaoban_right_hand").style.backgroundPositionX = 366 * 1 + "px"
      }
    }
  }
}];

model = models[1];
model.initModel();
