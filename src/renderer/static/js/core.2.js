const charlines = [
  {
    base: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '='],
    shift: ['!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+']
  }, {
    base: ['⇥', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '↵'],
    shift: ['⇤', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '{', '}', '↵']
  }, {
    base: ['a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\''],
    shift: ['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':', '"']
  }, {
    base: ['z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/'],
    shift: ['Z', 'X', 'C', 'V', 'B', 'N', 'M', '<', '>', '?']
  }
]

const special = [
  'esc', 'tab', 'caps', 'shift', 'fn', 'ctrl', 'alt', 'command', 'space', 'delete', 'enter'
]

const { ipcRenderer } = require("electron");

ipcRenderer.on("keyevent", (sender, e) => {
  if (e.type == "keyup") {
    document.getElementById("bailaoban_right_hand").style.backgroundPositionX = 0 + "px"
  } else {
    // console.log(e);
    if (e.keycode == 57416 || e.keycode == 17) {
      // 上
      document.getElementById("bailaoban_right_hand").style.backgroundPositionX = 366 * 3 + "px"
    } else if (e.keycode == 57424 || e.keycode == 31) {
      // 下
      document.getElementById("bailaoban_right_hand").style.backgroundPositionX = 366 * 4 + "px"
    } else if (e.keycode == 57419 || e.keycode == 30) {
      // 左
      document.getElementById("bailaoban_right_hand").style.backgroundPositionX = 366 * 2 + "px"
    } else if (e.keycode == 57421 || e.keycode == 32) {
      // 右
      document.getElementById("bailaoban_right_hand").style.backgroundPositionX = 366 * 1 + "px"
    }
  }
});