var AceEditor = {
  template: `
    <div style="height: 100px" id="editorDiv"></div>
  `,
  props: {
    modelValue: {
      type: String,
      default: ""
    }
  },
  watch: {
    modelValue(val) {
      // this.editor.setValue(val);
      // this.dispatch('ElFormItem', 'el.form.change', [val]);
    }
  },
  data() {
    return {
      editor: null
    }
  },
  mounted() {
    this.editor = ace.edit("editorDiv");
    this.editor.$blockScrolling = Infinity;
    //字体大小
    this.editor.setFontSize(16);
    //设置编辑器样式，对应theme-*.js文件
    this.editor.session.setMode("ace/mode/sh");
    //设置打印线是否显示
    this.editor.setShowPrintMargin(false);
    //设置是否只读
    this.editor.setReadOnly(false);
    //设置代码折叠
    this.editor.getSession().setUseWrapMode(true);
    //设置高亮
    //this.editor.setHighlightActiveLine(false);
    31 
    //以下部分是设置输入代码提示的，如果不需要可以不用引用ext-language_tools.js
    ace.require("ace/ext/language_tools");
    this.editor.setOptions({
        enableBasicAutocompletion: true,
        enableSnippets: true,
        enableLiveAutocompletion: true
    });
    this.editor.setTheme("ace/theme/chrome");
    this.editor.setValue(this.modelValue);
    this.editor.getSession().on('change', _=>{
      this.$emit('update:modelValue', this.editor.getValue());
    });
  },
};