var UserDefinedList = {
  template: `
    <el-card :body-style="{ padding: '0px' }">
      <template #header>
        <div class="card-header" style="-webkit-app-region: drag">
          <span>用户自定义脚本</span>
          <div>
            <el-button @click="createShellScript" type="success" icon="el-icon-plus" size="mini" circle></el-button>
            <el-button @click="closeWindow" type="danger" icon="el-icon-close" size="mini" circle></el-button>
          </div>
        </div>
      </template>
      <el-scrollbar height="380px">
      <el-table
        :data="datalist"
        style="width: 100%"
        :row-class-name="tableRowClassName"
      >
        <el-table-column label="状态" width="90">
          <template #default="scope">
            <span style="margin-left: 10px">{{ scope.row.active ? 'Active' : 'Stop' }}</span>
          </template>
        </el-table-column>
        <el-table-column prop="label" label="名称" width="150" />
        <el-table-column prop="isLoop" label="循环" width="157">
          <template  #default="scope">
            <el-switch :value="scope.row.isLoop == 1" />
          </template>
        </el-table-column>
        <el-table-column label="操作" width="200">
          <template  #default="scope">
            <el-button @click="editShellScript(scope.row, scope.row.active)" type="primary" round>修改</el-button>
            <el-button @click="deleteShellScript(scope.row.label, scope.row.active)" type="danger" round>删除</el-button>
          </template>
        </el-table-column>
      </el-table>
      </el-scrollbar>
    </el-card>
  `,
  components: {
    AceEditor
  },
  emit: {
    "turntopage": null
  },
  setup(props, {emit}) {
    const datalist = reactive([]);
    const active_row = ref(-1);

    /** ipc */
    initIpc(datalist, active_row);

    /** close window */
    const closeWindow = () => {
      ipcRenderer.send('window-close');
    };

    /** shell admin */
    const createShellScript = () => {
      emit("turntopage", "form");
    };
    const deleteShellScript = (res, flag) => {
      if(flag) {
        ElMessageBox.alert(
          "无法在脚本选定时删除！",
          "Waring",
          {
            confirmButtonText: 'OK',
            type: "waring",
            center: true,
          }
        )
      } else {
        ipcRenderer.send("shell-delete", res);
      }
    };
    const editShellScript = (res, flag) => {
      if(flag) {
        ElMessageBox.alert(
          "无法在脚本选定时修改！",
          "Waring",
          {
            confirmButtonText: 'OK',
            type: "waring",
            center: true,
          }
        )
      } else {
        emit("turntopage", {
          name: "form",
          data: {
            shell_name: res.label,
            result_reg: res.resultReg,
            shell_content: res.clickShell,
            is_loop: res.isLoop,
            loop_duration: res.loopDuration,
            color: res.color
          }
        });
      }
    };

    /** renderer style */
    const tableRowClassName = ({ rowIndex }) => {
      if (rowIndex === active_row.value) {
        return 'success-row'
      }
      return ''
    };

    return {
      datalist,
      active_row,
      closeWindow,
      createShellScript,
      deleteShellScript,
      editShellScript,
      tableRowClassName
    }
  }
};

function initIpc(datalist, active_row) {
  let ipc1 = (_, res) => {
    active_row.value = res;
    datalist.forEach((_, index) => {
      if(index == res) datalist[index].active = true;
      else datalist[index].active = false;
    });
  };
  let ipc2 = (_, res) => {
    datalist.length = 0;
    datalist.push(...res);
  };
  ipcRenderer.on("change-active-shell", ipc1);
  ipcRenderer.on("shell-data-get-reply", ipc2);
  ipcRenderer.on("shell-delete-reply", ipc2);
  ipcRenderer.send("shell-data-get", "userDefineShell");
  ipcRenderer.send("tip-change-active-shell");
  onUnmounted(() => {
    ipcRenderer.removeAllListeners("change-active-shell");
    ipcRenderer.removeAllListeners("shell-data-get-reply");
    ipcRenderer.removeAllListeners("shell-delete-reply");
  });
}