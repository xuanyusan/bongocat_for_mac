var UserDefinedForm = {
  template: `
    <el-card :body-style="{ padding: '0px' }">
      <template #header>
        <div class="card-header" style="-webkit-app-region: drag">
          <span>用户自定义脚本</span>
          <div>
            <el-button @click="back" type="primary" icon="el-icon-back" size="mini" circle></el-button>
            <el-button @click="onSubmit" type="success" icon="el-icon-check" size="mini" circle :loading="loading"></el-button>
            <el-button @click="closeWindow" type="danger" icon="el-icon-close" size="mini" circle></el-button>
          </div>
        </div>
      </template>
      <el-scrollbar height="380px">
        <el-form ref="form" :model="form" :rules="rules" label-width="100px" class="example-showcase">
          <el-collapse v-model="activeNames">
            <el-collapse-item title="&emsp;常规选项" name="1">
              <el-form-item label="脚本名称" prop="shell_name">
                <el-popover
                  ref="popover"
                  placement="top"
                  :width="420"
                  :visible="sn_pop_visible && sn_pop_visible_plus"
                >
                  <p>本应用依赖脚本名称作为脚本的唯一标识，请不要用重复的名称 <el-button @click="sn_pop_visible_plus=false" type="text" class="el-icon-close"></el-button></p>
                  <template #reference>
                    <el-input @focus="sn_pop_visible=true" @blur="sn_pop_visible=false" name="shell_name" v-model="form.shell_name" placeholder="design the shell script for yourself"></el-input>
                  </template>
                </el-popover>
              </el-form-item>
              <el-form-item label="脚本内容" props="shell_content" ref="ruleForm">
                <ace-editor v-model="form.shell_content"></ace-editor>
              </el-form-item>
              <el-form-item label="脚本测试">
                <el-row>
                  <el-col :span="6">
                    <el-button @click="testshell" type="primary" icon="el-icon-video-play" circle :loading="shell_test_loading"></el-button>
                  </el-col>
                  <el-col :span="18">
                    <el-input v-model="form.result_reg" placeholder="\\d+">
                      <template #prepend>正则匹配</template>
                    </el-input>
                  </el-col>
                </el-row>
              </el-form-item>
              <el-form-item label="循环执行">
                <el-row>
                  <el-col :span="6">
                    <el-radio v-model="form.is_loop" label="1">循环执行</el-radio>
                  </el-col>
                  <el-col :span="18">
                    <el-input v-model="form.loop_duration" :disabled="!form.is_loop">
                      <template #prepend>循环间隙</template>
                    </el-input>
                  </el-col>
                </el-row>
              </el-form-item>
            </el-collapse-item>
            <el-collapse-item title="&emsp;高级选项" name="2">
              <el-form-item label="输出颜色">
                <el-select v-model="form.color">
                  <el-option
                    v-for="item in form.colors"
                    :key="item.num"
                    :label="item.label"
                    :value="item.num"
                  >
                  </el-option>
                </el-select>
              </el-form-item>
              <el-form-item label="提示信息">
                <el-input v-model="form.tooltip" type="textarea" :rows="3" :autosize="false"></el-input>
              </el-form-item>
              <el-form-item label="挂起信息">
                <el-input v-model="form.up_message"></el-input>
              </el-form-item>
            </el-collapse-item>
          </el-collapse>
        </el-form>
      </el-scrollbar>
    </el-card>
  `,
  components: {
    AceEditor
  },
  props: {
    initdata: {
      type: Object,
      default: {}
    }
  },
  data() {
    return {
      form: {
        shell_name: '',
        tooltip: "",
        result_reg: '',
        shell_content: '',
        is_loop: false,
        loop_duration: 1000,
        colors: [
          { num: '30', color: 'black', label: '黑色' },
          { num: '31', color: 'red', label: '红色' },
          { num: '32', color: 'limegreen', label: '绿色' },
          { num: '33', color: 'yellow', label: '黄色' },
          { num: '34', color: 'blue', label: '蓝色' },
          { num: '35', color: 'purse', label: '紫色' },
          { num: '36', color: 'cornflowerblue', label: '青色' },
          { num: '37', color: 'white', label: '白色' }
        ],
        color: "30",
        up_message: ''
      },
      rules: {
        shell_name: [
          {
            required: true,
            message: '请输入名称',
            trigger: 'blur',
          },
          {
            min: 3,
            max: 15,
            message: '名称长度在3-15之间',
            trigger: 'blur',
          }
        ],
        shell_content: [
          {
            required: true,
            message: '请输入内容',
            trigger: 'change'
          }
        ],
      },
      loading: false,
      shell_test_loading: false,
      activeNames: "1",
      sn_pop_visible: false,
      sn_pop_visible_plus: true
    }
  },
  created() {
    for(let key in this.initdata) {
      this.form[key] = this.initdata[key];
    }
    ipcRenderer.removeAllListeners("shell-test-reply");
    ipcRenderer.on("shell-test-reply", this.ipc1);
  },
  methods: {
    back() {
      this.$emit("turnto", "list");
    },
    onSubmit() {
      let { shell_name, tooltip, shell_content, result_reg, is_loop, loop_duration, color, up_message } = this.form;
      ipcRenderer.send("shell-put", {
        label: shell_name,
        tooltip: tooltip,
        clickShell: shell_content,
        resultReg: result_reg,
        color: color,
        isLoop: is_loop,
        loopDuration: loop_duration,
        isUp: !!(up_message),
        upMessage: up_message
      });
      this.$alert(
        "脚本保存成功",
        "Success",
        {
          confirmButtonText: 'OK',
          type: "success",
          center: true,
        })
    },
    changeShellContent(val) {
      this.form.shell_content = val;
    },
    testshell() {
      this.shell_test_loading = true;
      ipcRenderer.send("shell-test", {
        shell_content: this.form.shell_content,
        result_reg: this.form.result_reg
      });
    },
    closeWindow() {
      ipcRenderer.send('window-close');
    },
    ipc1(_, res) {
      this.shell_test_loading = false;
      this.$alert(
        `<div style="background-color: grey;color: white;text-align: left;padding: 5px;border-radius: 5px">
          <pre>${res.result.split('\n').map(item=>`<code>${item}</code>`).join('\n')}</pre>
        </div>`,
        res.message,
        {
          dangerouslyUseHTMLString: true,
          confirmButtonText: 'OK',
          type: res.message,
          center: true,
        }
      );
    }
  }
};